package com.proof.apps.controller;

import com.proof.solution.clients.application.create.ClientCreator;
import com.proof.solution.clients.application.create.CreateClientRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ClientPutController {
    private ClientCreator creator;

    public ClientPutController(ClientCreator creator) {
        this.creator = creator;
    }
    @CrossOrigin(origins = "http://localhost:4200")
    @PutMapping("/clients/{id}")
    public ResponseEntity create(@PathVariable String id, @RequestBody Request request) {
        creator.create(new CreateClientRequest(
            id,
            request.documentType(),
            request.documentNumber(),
            request.email(),
            request.phone()
        ));

        return new ResponseEntity(HttpStatus.CREATED);
    }


}

final class Request {
    public String documentType;
    public String documentNumber;
    public String email;
    public String phone;

    public String documentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String documentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String email() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String phone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
