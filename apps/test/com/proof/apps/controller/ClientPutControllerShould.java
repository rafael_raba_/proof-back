package com.proof.apps.controller;


import com.proof.apps.ApplicationTestCase;
import org.junit.jupiter.api.Test;

final class ClientPutControllerShould extends ApplicationTestCase {
    @Test
    void create_valid_client() throws Exception {
        assertRequestWithBody(
                "PUT",
                "/clients/b5fa9283-d62f-44ef-8661-c7fa6a2720cc",
                "{\"documentType\":\"CC\",\"documentNumber\":\"10306074545\",\"email\":\"rafaelrb40@gmail.com\",\"phone\":\"3002834744\"}",
                201
        );
    }
}