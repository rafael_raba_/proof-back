package com.proof.solution.clients.application.create;


public class CreateClientRequest {
    public String documentType;
    public String id;
    public String documentNumber;
    public String email;
    public String phone;

    public CreateClientRequest(String id,String documentType, String documentNumber, String email, String phone) {
        this.id = id;
        this.documentType = documentType;
        this.documentNumber = documentNumber;
        this.email = email;
        this.phone = phone;
    }



    public String id() {
        return id;
    }

    public String documentType() {
        return documentType;
    }

    public String documentNumber() {
        return documentNumber;
    }

    public String email() {
        return email;
    }

    public String phone() {
        return phone;
    }
}
