package com.proof.solution.clients.application.create;

import com.proof.solution.clients.domain.*;
import org.springframework.stereotype.Service;

@Service
public class ClientCreator {
    private ClientRepository repository;

    public ClientCreator(ClientRepository repository) {
        this.repository = repository;
    }

    public void create(CreateClientRequest request) {
        Client client = new Client(
            new ClientId(request.id()),
            new ClientDocumentType(request.documentType()),
            new ClientDocumentNumber(request.documentNumber()),
            new ClientEmail(request.email()),
            new ClientPhone(request.phone())
        );
        repository.save(client);
    }
}
