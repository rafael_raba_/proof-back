package com.proof.solution.clients.domain;

import com.proof.shared.domain.StringValueObject;

public class ClientPhone extends StringValueObject {

    public ClientPhone(String value) {
        super(value);
        ensureLength();
    }

    private ClientPhone() {
        super("");
    }

    private void ensureLength() {
    }
}
