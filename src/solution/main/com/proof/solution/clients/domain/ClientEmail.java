package com.proof.solution.clients.domain;

import com.proof.shared.domain.StringValueObject;

public class ClientEmail extends StringValueObject {
    public ClientEmail(String value) {
        super(value);
    }

    private ClientEmail() {
        super("");
    }
}
