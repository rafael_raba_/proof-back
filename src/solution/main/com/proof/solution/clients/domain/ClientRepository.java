package com.proof.solution.clients.domain;

public interface ClientRepository {
    void save(Client client);
}
