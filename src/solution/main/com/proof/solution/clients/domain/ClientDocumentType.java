package com.proof.solution.clients.domain;

import com.proof.shared.domain.StringValueObject;

public class ClientDocumentType extends StringValueObject {
    public ClientDocumentType(String value) {
        super(value);
    }

    private ClientDocumentType() {
        super("");
    }
}
