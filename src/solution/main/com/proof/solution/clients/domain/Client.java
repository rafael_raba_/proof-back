package com.proof.solution.clients.domain;

import java.util.Objects;

public class Client {
    ClientId id;
    ClientDocumentType documentType;
    ClientDocumentNumber documentNumber;
    ClientEmail email;
    ClientPhone phone;

    public Client(ClientId id, ClientDocumentType documentType, ClientDocumentNumber documentNumber, ClientEmail email, ClientPhone phone) {
        this.id = id;
        this.documentType = documentType;
        this.documentNumber = documentNumber;
        this.email = email;
        this.phone = phone;
    }

    public ClientId id() {
        return id;
    }

    public ClientDocumentType documentType() {
        return documentType;
    }

    public ClientDocumentNumber documentNumber() {
        return documentNumber;
    }

    public ClientEmail email() {
        return email;
    }

    public ClientPhone phone() {
        return phone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return id.equals(client.id) &&
            documentType.equals(client.documentType) &&
            documentNumber.equals(client.documentNumber) &&
            email.equals(client.email) &&
            phone.equals(client.phone);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, documentType, documentNumber, email, phone);
    }
}
