package com.proof.solution.clients.domain;

import com.proof.shared.domain.StringValueObject;

public class ClientDocumentNumber extends StringValueObject {

    public ClientDocumentNumber(String value) {
        super(value);
    }

    private ClientDocumentNumber() {
        super("");
    }
}
