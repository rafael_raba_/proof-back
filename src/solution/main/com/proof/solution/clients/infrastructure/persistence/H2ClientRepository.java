package com.proof.solution.clients.infrastructure.persistence;

import com.proof.solution.clients.domain.Client;
import com.proof.solution.clients.domain.ClientRepository;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class H2ClientRepository implements ClientRepository {
    private SessionFactory sessionFactory;

    public H2ClientRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    @Transactional
    public void save(Client client) {
        sessionFactory.getCurrentSession().save(client);
    }
}
