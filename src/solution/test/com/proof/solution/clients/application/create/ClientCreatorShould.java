package com.proof.solution.clients.application.create;

import com.proof.solution.clients.ClientsModuleUnitTestCase;
import com.proof.solution.clients.domain.Client;

import org.junit.jupiter.api.BeforeEach;
import com.proof.solution.clients.domain.*;
import org.junit.jupiter.api.Test;

public class ClientCreatorShould extends ClientsModuleUnitTestCase {
    private ClientCreator creator;

    @BeforeEach
    protected void setUp() {
        super.setUp();
        creator = new ClientCreator(repository);
    }

    @Test
    public void create_a_valid_client() {
        CreateClientRequest request =CreateClientRequestMother.random();
        System.out.println(request.documentType);
        Client client = ClientMother.fromRequest(request);
        creator.create(request);
        shouldHaveSaved(client);

    }
}
