package com.proof.solution.clients.application.create;

import com.proof.solution.clients.domain.*;

public class CreateClientRequestMother {

    public static CreateClientRequest create(
        ClientId id,
        ClientDocumentType documentType,
        ClientDocumentNumber documentNumber,
        ClientEmail email,
        ClientPhone phone
    ) {
        return new CreateClientRequest(
            id.value(),
            documentType.value(),
            documentNumber.value(),
            email.value(),
            phone.value());
    }

    public static CreateClientRequest random() {
        return create(
            ClientIdMother.random(),
            ClientDocumentTypeMother.random(),
            ClientDocumentNumberMother.random(),
            ClientEmailMother.random(),
            ClientPhoneMother.random());
    }


}
