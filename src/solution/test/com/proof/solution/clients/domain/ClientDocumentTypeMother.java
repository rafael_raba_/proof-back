package com.proof.solution.clients.domain;


import com.proof.shared.domain.WordMother;

public class ClientDocumentTypeMother {
    public static ClientDocumentType create(String value) {
        return new ClientDocumentType(value);
    }

    public static ClientDocumentType random() {
        return create(WordMother.random());
    }
}
