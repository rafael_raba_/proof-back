package com.proof.solution.clients.domain;

import com.proof.shared.domain.WordMother;

public class ClientDocumentNumberMother {
    public static ClientDocumentNumber create(String value) {
        return new ClientDocumentNumber(value);
    }

    public static ClientDocumentNumber random() {
        return create(WordMother.random());
    }
}
