package com.proof.solution.clients.domain;

import com.proof.shared.domain.WordMother;

public class ClientEmailMother {
    public static ClientEmail create(String value) {
        return new ClientEmail(value);
    }

    public static ClientEmail random() {
        return create(WordMother.random());
    }
}
