package com.proof.solution.clients.domain;

import com.proof.solution.clients.application.create.CreateClientRequest;

public class ClientMother {
    public static Client create(
        ClientId id,
        ClientDocumentType documentType,
        ClientDocumentNumber documentNumber,
        ClientEmail email,
        ClientPhone phone
    ) {
        return new Client(id, documentType, documentNumber, email, phone);
    }

    public static Client fromRequest(CreateClientRequest request) {
        return create(
            ClientIdMother.create(request.id()),
            ClientDocumentTypeMother.create(request.documentType()),
            ClientDocumentNumberMother.create(request.documentNumber()),
            ClientEmailMother.create(request.email()),
            ClientPhoneMother.create(request.phone())
        );
    }
    public static Client random() {
        return create(
            ClientIdMother.random(),
            ClientDocumentTypeMother.random(),
            ClientDocumentNumberMother.random(),
            ClientEmailMother.random(),
            ClientPhoneMother.random()
        );
    }
}
