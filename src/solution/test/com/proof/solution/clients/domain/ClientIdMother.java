package com.proof.solution.clients.domain;

import com.proof.shared.domain.WordMother;

public class ClientIdMother {

    public static ClientId create(String value) {
        return new ClientId(value);
    }

    public static ClientId random() {
        return create(WordMother.randomUuId());
    }
}
