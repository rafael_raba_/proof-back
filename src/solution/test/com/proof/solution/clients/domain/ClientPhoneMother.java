package com.proof.solution.clients.domain;

import com.proof.shared.domain.WordMother;

public class ClientPhoneMother {
    public static ClientPhone create(String value) {
        return new ClientPhone(value);
    }

    public static ClientPhone random() {
        Long number = WordMother.randomInt();
        String random = Long.toString(number);
        return create(random);
    }
}
