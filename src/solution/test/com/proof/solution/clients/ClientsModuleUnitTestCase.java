package com.proof.solution.clients;

import com.proof.solution.clients.domain.Client;
import com.proof.solution.clients.domain.ClientRepository;
import org.junit.jupiter.api.BeforeEach;

import static org.mockito.Mockito.*;

public class ClientsModuleUnitTestCase {
    protected ClientRepository repository;

    @BeforeEach
    protected void setUp() {
        repository = mock(ClientRepository.class);
    }

    protected void shouldHaveSaved(Client client) {
        verify(repository, atLeastOnce()).save(client);
    }
}
