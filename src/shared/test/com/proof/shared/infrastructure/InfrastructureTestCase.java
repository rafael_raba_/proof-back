package com.proof.shared.infrastructure;

import com.proof.apps.Starter;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(classes = Starter.class)
@SpringBootTest
public class InfrastructureTestCase {
}
