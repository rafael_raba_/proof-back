package com.proof.shared.domain;


import com.github.javafaker.Faker;

import java.util.UUID;

public class WordMother {

    public static String random() {
        return Faker.instance().lorem().word();
    }
    public static long randomInt() {
        Faker faker = new Faker();
        return faker.number().randomNumber(10, true );
    }

    public static String randomUuId() {
        return UUID.randomUUID().toString();
    }

}
